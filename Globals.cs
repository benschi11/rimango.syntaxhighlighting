﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rimango.SyntaxHighlighting
{
    public static class Globals
    {
        public struct Script {
            public const string Core = "Rimango.SyntaxHighlighting.Core";
            public const string AutoLoader = "Rimango.SyntaxHighlighting.AutoLoader";
            public const string Init = "Rimango.SyntaxHighlighting.Init";

        }

        public struct Style
        {
            public const string Core = "Rimango.SyntaxHighlighting.Core";
            public const string DefaultTheme = "Rimango.SyntaxHighlighting.DefaultTheme";

        }

        public const string BaseUrlToken = "{BaseUrl}";
    }
}