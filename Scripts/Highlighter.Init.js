﻿SyntaxHighlighter.autoloader(
	'applescript			{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushAppleScript.js',
	'actionscript3 as3		{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushAS3.js',
	'bash shell				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushBash.js',
	'coldfusion cf			{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushColdFusion.js',
	'cpp c					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushCpp.js',
	'c# c-sharp csharp		{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushCSharp.js',
	'css					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushCss.js',
	'delphi pascal			{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushDelphi.js',
	'diff patch pas			{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushDiff.js',
	'erl erlang				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushErlang.js',
	'groovy					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushGroovy.js',
	'haxe hx				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushHaxe.js',
	'java					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushJava.js',
	'jfx javafx				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushJavaFX.js',
	'js jscript javascript	{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushJScript.js',
	'perl pl				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushPerl.js',
	'php					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushPhp.js',
	'text plain				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushPlain.js',
	'py python				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushPython.js',
	'ruby rails ror rb		{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushRuby.js',
	'scala					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushScala.js',
	'sql					{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushSql.js',
	'vb vbnet				{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushVb.js',
	'xml xhtml xslt html	{BaseUrl}/Modules/Rimango.SyntaxHighlighting/Scripts/syntaxhighlighter/shBrushXml.js'
);


SyntaxHighlighter.all();