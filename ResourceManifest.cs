﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.UI.Resources;

namespace Rimango.SyntaxHighlighting
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();

            manifest.DefineStyle(Globals.Style.Core).SetUrl("syntaxhighlighter/shCore.css");
            manifest.DefineStyle(Globals.Style.DefaultTheme).SetUrl("syntaxhighlighter/shThemeEclipse.css");


            manifest.DefineScript(Globals.Script.Core).SetUrl("syntaxhighlighter/shCore.js");
            manifest.DefineScript(Globals.Script.AutoLoader).SetUrl("syntaxhighlighter/shAutoloader.js").SetDependencies(Globals.Script.Core);
            manifest.DefineScript(Globals.Script.Init).SetUrl("Highlighter.Init.js").SetDependencies(Globals.Script.AutoLoader);

        }
    }
}