﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Orchard;
using Orchard.FileSystems.VirtualPath;
using Orchard.Mvc.Filters;
using Orchard.UI.Resources;

namespace Rimango.SyntaxHighlighting.Filters
{
    public class HighlighterInjectFilter : FilterProvider, IResultFilter
    {
        private readonly IResourceManager _resourceManager;
        private readonly IOrchardServices _orchardServices;
        private readonly IVirtualPathProvider _virtualPathProvider;

        public HighlighterInjectFilter(
            IResourceManager resourceManager, 
            IOrchardServices orchardServices, 
            IVirtualPathProvider virtualPathProvider) {

            _resourceManager = resourceManager;
            _orchardServices = orchardServices;
            _virtualPathProvider = virtualPathProvider;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext) {

            // Should only run on a full view rendering result
            if (!(filterContext.Result is ViewResult))
                return;

            // Include the following script on all pages
            _resourceManager.Require("script", Globals.Script.AutoLoader).AtFoot();
            _resourceManager.Require("stylesheet", Globals.Style.Core).AtHead();
            _resourceManager.Require("stylesheet", Globals.Style.DefaultTheme).AtHead();

            _resourceManager.RegisterFootScript(GetInitScript());
        }

        private string GetInitScript() {
            var sb = new StringBuilder();
            sb.Append("<script type='text/javascript'>\n");
            var path = _virtualPathProvider.MapPath("~/Modules/Rimango.SyntaxHighlighting/Scripts/Highlighter.Init.js");
            var text = File.ReadAllText(path);
            text = text.Replace(Globals.BaseUrlToken, _orchardServices.WorkContext.CurrentSite.BaseUrl);
            sb.Append(text);
            sb.Append("\n</script>");
            return sb.ToString();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext) {
        }
    }
}